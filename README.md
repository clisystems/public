![CLISystems](https://clisystems.com/img/logohub_black_width1000px.png){width=600}

# CLI Systems Public Repo

CLI Systems is firmware experts! CLI Systems is a product development group in Colorado, USA specializing in product design, embedded system design, and firmware development.  This public repository is for code and system solutions we share with the world.

## Repository Contents

- Folders in root directory contain code and documentation for a specific project
- /sandbox - This is our 'staging' area, code and projects in here may not be documented
- /docs - Information related to the projects in the repo

## Requests & Contact

Please contact info@clisystems.com for any questions or concerns related to the code in this repository
