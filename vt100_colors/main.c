#include <stdio.h>
#include <stdint.h>

#define VT100ESC    0x1B
#define RED()       printf("%c[91m", VT100ESC)
#define GREEN()     printf("%c[92m", VT100ESC)
#define YELLOW()    printf("%c[93m", VT100ESC)
#define BLUE()      printf("%c[94m", VT100ESC)
#define MAGENTA()   printf("%c[95m", VT100ESC)
#define CYAN()      printf("%c[96m", VT100ESC)
#define RESET()     printf("%c[0m", VT100ESC)

int main(int cargc, char ** argv)
{
    printf("Running...\n");
    RED();printf("This is red!\n");
    GREEN();printf("This is green!\n");
    BLUE();printf("This is blue!\n");
    YELLOW();printf("This is yellow!\n");
    MAGENTA();printf("This is magenta!\n");
    CYAN();printf("This is cyan!\n");
    RESET();
    return 0;
}
