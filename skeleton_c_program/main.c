#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

// Definitions
// ----------------------------------------------------------------------------

// Types and enums
// ----------------------------------------------------------------------------

// Variables
// ----------------------------------------------------------------------------
bool g_running = false;

// Local prototypes
// ----------------------------------------------------------------------------

// Functions
// ----------------------------------------------------------------------------

// Main function
// ----------------------------------------------------------------------------
int main(int argc,char** argv)
{    
    // Setup system
    g_running = true;
 


    // Handle data from stdin
    printf("System started\n");
	while(g_running)
	{
		// Do things
        g_running = false;
		
	} // end while
	
    // Should not get here for embedded systems
    // For Linux systems, shutdown code modules before exit
	
	// Shutdown system	
    printf("Normal exit\n");
    return 0;
}

// EOF
